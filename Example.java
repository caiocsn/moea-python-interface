/*
* This code is an adaptation of MOEA Framework Example4
* Author: Caio Nunes
*/

/* Copyright 2009-2016 David Hadka
 *
 * This file is part of the MOEA Framework.
 *
 * The MOEA Framework is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * The MOEA Framework is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with the MOEA Framework.  If not, see <http://www.gnu.org/licenses/>.
 */
import org.moeaframework.Executor;
import org.moeaframework.core.NondominatedPopulation;
import org.moeaframework.core.Solution;
import org.moeaframework.core.variable.EncodingUtils;
import org.moeaframework.core.variable.RealVariable;
import org.moeaframework.problem.AbstractProblem;

/**
 * Demonstrates how a new problem is defined and used within the MOEA
 * Framework.
 */
public class Example7 {

    /**
     * Implementation of the a problem based in 
     * a python script.
     */
    public static class MyProblem extends AbstractProblem {

        /**
         * Constructs a new instance of your function, defining it
         * to include 50 decision variables and 2 objectives.
         */
        public MyProblem() {
            super(50, 2);
        }

        /**
         * Constructs a new solution and defines the bounds of the decision
         * variables.
         */
        @Override
        public Solution newSolution() {
            Solution solution = new Solution(getNumberOfVariables(),
                    getNumberOfObjectives());

            for (int i = 0; i < getNumberOfVariables(); i++) {
                solution.setVariable(i, new RealVariable(0.0, 1.0));
            }

            return solution;
        }

        /**
         * Extracts the decision variables from the solution, evaluates the
         * Rosenbrock function, and saves the resulting objective value back to
         * the solution.
         */
        @Override
        public void evaluate(Solution solution) {
            double[] x = EncodingUtils.getReal(solution);
            double[] f;

            String PATH = "/home/caio/Downloads/MOEAFramework-2.12-Source/MOEAFramework-2.12/examples/myfunc.py";
            String functionName = "target_function";
            f = PythonInterface.PyEval(numberOfObjectives, numberOfVariables, x, PATH, functionName);
            int k = numberOfVariables - numberOfObjectives + 1;

            solution.setObjectives(f);
        }

    }

    public static void main(String[] args) {
        //configure and run our problem
        NondominatedPopulation result = new Executor()
                .withProblemClass(MyProblem.class)
                .withAlgorithm("NSGAII")
                .withMaxEvaluations(100)
                .run();

        //display the results
        System.out.format("Objective1  Objective2%n");

        for (Solution solution : result) {
            System.out.format("%.4f      %.4f%n",
                    solution.getObjective(0),
                    solution.getObjective(1));
        }
    }

}


        //display the results
        System.out.format("Objective1  Objective2%n");

        for (Solution solution : result) {
            System.out.format("%.4f      %.4f%n",
                    solution.getObjective(0),
                    solution.getObjective(1));
        }
    }

}
