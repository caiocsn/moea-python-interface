import org.python.util.PythonInterpreter;
import org.python.core.*; 
import java.util.*;


public class PythonInterface{
	public static double[] PyEval(int numberOfObjectives, int numberOfVariables, double[] solution, String PATH, String functionName) {

		Locale.setDefault(Locale.US);

		PythonInterpreter python = new PythonInterpreter();

		double[] Y = new double[numberOfObjectives];

 		python.set("numberOfVariables", new PyInteger(numberOfVariables));
		python.set("numberOfObjectives", new PyInteger(numberOfObjectives));
		python.exec("X = []");

		for(int i = 0; i < numberOfVariables ; i++)
		{
			python.exec(String.format("X.append(%f)", solution[i]));
		}

		python.execfile(PATH);
		PyObject result = python.eval(String.format("%s(X)",functionName));

		String aux = result.toString().replace("[", "").replace("]","");
		String tokens[] = aux.split(",");

		for(int i = 0; i < numberOfObjectives; i ++)
			Y[i] = Double.parseDouble(tokens[i]);


		return Y;
	}
	public static void main(String[] args) {
		int numberOfObjectives = 2;
		int numberOfVariables = 4;
		double[] solution = {1,1,1,1};
		String PATH = "/home/caio/Downloads/MOEAFramework-2.12-Source/MOEAFramework-2.12/examples/myfunc.py";
		String functionName = "target_function";

		double[] result = new double[numberOfObjectives];
		result = PyEval(numberOfObjectives, numberOfVariables, solution, PATH, functionName);

		for(int i = 0; i < numberOfObjectives; i++)
			System.out.println(result[i]);

	}

	}
